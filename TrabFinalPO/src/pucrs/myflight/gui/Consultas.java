package pucrs.myflight.gui;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import org.jxmapviewer.viewer.GeoPosition;

import javafx.scene.control.TextField;
import pucrs.myflight.modelo.Aeroporto;
import pucrs.myflight.modelo.Geo;
import pucrs.myflight.modelo.GerenciadorAeroportos;
import pucrs.myflight.modelo.GerenciadorRotas;
import pucrs.myflight.modelo.Rota;


public class Consultas {
	
	private GerenciadorAeroportos gerAero;
	private GerenciadorRotas gerRotas;
	private GerenciadorMapa gerenciador;
	private ArrayList<Aeroporto> aeroportos;
	private ArrayList<Rota> rotas;
	
	public Consultas(GerenciadorMapa gerenciador, GerenciadorAeroportos gerAero, GerenciadorRotas gerRotas){
		this.gerAero = gerAero;
		this.gerRotas = gerRotas;
		this.gerenciador = gerenciador;
	}
	public void Consulta1(){
		gerenciador.clear();
	    GeoPosition pos = gerenciador.getPosicao();  
	    List<MyWaypoint> lista = new ArrayList<>(); 
	    String pais = "XXXXXXXX";
	    aeroportos = gerAero.listarTodos();
	    for(Aeroporto a : aeroportos) {
	    	if(a.getLocal().getLatitude()>(pos.getLatitude()-0.20) && a.getLocal().getLatitude()<(pos.getLatitude()+0.20) && a.getLocal().getLongitude()>(pos.getLongitude()-0.20)&&a.getLocal().getLongitude()<(pos.getLongitude()+0.20)){
	    		pais = a.getPaisSigla();
	    	}else{
	    		
	    	}	
	    }
	    for(Aeroporto b : aeroportos) {
	    	if(b.getPaisSigla().contains(pais)){
	    		Geo gg = b.getLocal();
	    		lista.add(new MyWaypoint(Color.BLUE, b.getNome(), gg));
	    	}
	    }
	    if(pais == "XXXXXXXX"){
	    	System.out.println("nao achou");
	    }
 
	    gerenciador.setPontos(lista);
	    gerenciador.getMapKit().repaint();
	}
	public void Consulta2(TextField DistanciaTextField){
	 	gerenciador.clear();
	 	Tracado tr = new Tracado();
	    GeoPosition pos = gerenciador.getPosicao();  
	    aeroportos = gerAero.listarTodos();
	    rotas = gerRotas.listarTodas();
	    List<MyWaypoint> lista = new ArrayList<>(); 
	    String origem = "XXXXXXXX";
	    int areaMaxima = Integer.parseInt(DistanciaTextField.getText());
	    areaMaxima = areaMaxima/100; //tranformando em decimal
	    
	    for(Aeroporto a : aeroportos) {
	    	if(a.getLocal().getLatitude()>(pos.getLatitude()-0.20) && a.getLocal().getLatitude()<(pos.getLatitude()+0.20) && a.getLocal().getLongitude()>(pos.getLongitude()-0.20)&&a.getLocal().getLongitude()<(pos.getLongitude()+0.20)){
	    		origem = a.getCodigo();
	    	}
	    }
	    for(Rota r : rotas) {
	    	if(r.getOrigem().getCodigo() == origem){
	    		Geo gg1 = r.getOrigem().getLocal();
		    	Geo gg2 = r.getDestino().getLocal();
		    	if(r.getOrigem().getLocal().getLatitude()>(r.getDestino().getLocal().getLatitude()-areaMaxima) && r.getOrigem().getLocal().getLatitude()<(r.getDestino().getLocal().getLatitude()+areaMaxima) && r.getOrigem().getLocal().getLongitude()>(r.getDestino().getLocal().getLongitude()-areaMaxima)&&r.getOrigem().getLocal().getLongitude()<(r.getDestino().getLocal().getLongitude()+areaMaxima)){
		    		double distancia = 0;
		    		distancia = gg1.distancia(gg2);
		    		String distanciaCompactada = String.format("%.2f", distancia);
		    		String label = "Aeroporto: " + r.getDestino().getNome() + ", Distancia: " + distanciaCompactada + "KM" + ", Aeronave: " + r.getAeronave().getDescricao();
		    		lista.add(new MyWaypoint(Color.BLUE, r.getOrigem().getNome(), gg1));
			    	lista.add(new MyWaypoint(Color.BLUE, label, gg2));
			    	tr.addPonto(gg1);
			        tr.addPonto(gg2);
			        tr.setCor(Color.RED);
			        gerenciador.addTracado(tr);	
		    	}
		    	
	    	}
	    	
	    }
	    if(origem == "XXXXXXXX"){
	    	System.out.println("nao achou");
	    }
 
	    gerenciador.setPontos(lista);
	    gerenciador.getMapKit().repaint();
	

	}
	public void Consulta3(){
		gerenciador.clear();
		Tracado tr = new Tracado();
		GeoPosition pos = gerenciador.getPosicao();  
	    aeroportos = gerAero.listarTodos();
	    rotas = gerRotas.listarTodas();
	    List<MyWaypoint> lista = new ArrayList<>(); 
	    ArrayList<String> guardaRota = new ArrayList<>();
	    String origem = "XXXXXXXX";
	    String destino = "XXXXXXXX";
	    for(Aeroporto a : aeroportos) {
	    	if(a.getLocal().getLatitude()>(pos.getLatitude()-0.20) && a.getLocal().getLatitude()<(pos.getLatitude()+0.20) && a.getLocal().getLongitude()>(pos.getLongitude()-0.20)&&a.getLocal().getLongitude()<(pos.getLongitude()+0.20)){
	    		origem = a.getCodigo();
	    	}else{
	    		
	    	}	
	    }
	    for(Rota r : rotas) {
	    	if(r.getOrigem().getCodigo() == origem){
		    	Geo gg1 = r.getOrigem().getLocal();
		    	Geo gg2 = r.getDestino().getLocal();
		    	double distancia = 0;
	    		distancia = gg1.distancia(gg2);
	    		String distanciaCompactada = String.format("%.2f", distancia);
	    		String label1 = "Aeroporto: " + r.getDestino().getNome() + ", Aeronave: " + r.getAeronave().getDescricao();
	    		String label2 = "Aeroporto: " + r.getDestino().getNome() + ", Distancia: " + distanciaCompactada + "KM" + ", Aeronave: " + r.getAeronave().getDescricao();
	    		lista.add(new MyWaypoint(Color.BLACK, label1, gg1));
			    tr.addPonto(gg1);
			    if(!guardaRota.contains(r.getDestino().getCodigo())){
			    	lista.add(new MyWaypoint(Color.GREEN, label2, gg2));
					tr.addPonto(gg2);
					destino = r.getDestino().getCodigo();
			    }else{
			    	destino = "XXXXXXXX";
			    }
		        tr.setCor(Color.yellow);
		        gerenciador.addTracado(tr);	
		        
		        int count = 1;
		        guardaRota.add(r.getOrigem().getCodigo());
			    for(Rota r2 : rotas){
			    	if(r2.getOrigem().getCodigo() == destino){
			    		if(!guardaRota.contains(r2.getOrigem().getCodigo())&&!guardaRota.contains(r2.getDestino().getCodigo())){
				    		Geo gg3 = r2.getOrigem().getLocal();
				    		Geo gg4 = r2.getDestino().getLocal();
				    		distancia = gg3.distancia(gg4);
				    		distanciaCompactada = String.format("%.2f", distancia);
				    		String label3 = "Aeroporto: " + r2.getDestino().getNome() + ", Distancia: " + distanciaCompactada + "KM" + ", Aeronave: " + r2.getAeronave().getDescricao();
				    		lista.add(new MyWaypoint(Color.GREEN, label3, gg3));
				    		lista.add(new MyWaypoint(Color.MAGENTA, label3, gg4));
						    tr.addPonto(gg3);
						    tr.addPonto(gg4);
						    guardaRota.add(r2.getOrigem().getCodigo());
						    guardaRota.add(r2.getDestino().getCodigo());
						    if(count == 2){
						    	break;
						    }else{
						    	count++;
						    }
				    	}
			    	}
			    	
			    }
	    	} 
		}
        
	    if(origem == "XXXXXXXX"){
	    	System.out.println("nao achou");
	    }
 
	    gerenciador.setPontos(lista);
	    gerenciador.getMapKit().repaint();
	}
	public void Consulta4(Object o){
		System.out.println(o.toString());
		gerenciador.clear();
		Tracado tr = new Tracado();
		rotas = gerRotas.listarTodas();
		List<MyWaypoint> lista = new ArrayList<>(); 
		for(Rota r : rotas) {
	    	if(r.getCia().getNome().contains(o.toString())){
		    	Geo gg1 = r.getOrigem().getLocal();
		    	Geo gg2 = r.getDestino().getLocal();
		    	double distancia = 0;
	    		distancia = gg1.distancia(gg2);
	    		String distanciaCompactada = String.format("%.2f", distancia);
	    		String label = "Aeroporto: " + r.getDestino().getNome() + ", Distancia: " + distanciaCompactada + "KM" + ", Aeronave: " + r.getAeronave().getDescricao();
		    	lista.add(new MyWaypoint(Color.BLACK, r.getOrigem().getNome(), gg1));
		    	lista.add(new MyWaypoint(Color.BLACK, label, gg1));
		    	tr.addPonto(gg1);
		        tr.addPonto(gg2);
		        tr.setCor(Color.green);
		        gerenciador.addTracado(tr);	
	    	}
		}
		gerenciador.setPontos(lista);
	    gerenciador.getMapKit().repaint();
	}
	
}
