package pucrs.myflight.modelo;

public class Country {
	private String codigo;
	private String nome;
	private static int totalCountry = 0;
	public Country(String codigo, String nome){
		this.codigo = codigo;
		this.nome = nome;
		totalCountry++;
	}
	public String getCodigo(){
		return codigo;
	}
	public String getNome(){
		return nome;
	}
	public int totalCountrys(){
		return totalCountry;
	}
}
