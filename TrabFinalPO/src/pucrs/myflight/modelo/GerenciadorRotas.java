package pucrs.myflight.modelo;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class GerenciadorRotas {

	private ArrayList<Rota> rotas;
	
	public GerenciadorRotas() {
		rotas = new ArrayList<>();
	}
	
	public void adicionar(Rota r) {
		rotas.add(r);	
	}
	
	public void carregaRotas(GerenciadorCias gerCias, GerenciadorAeroportos gerAero, GerenciadorAeronaves GerAer) throws IOException{
		Path path2 = Paths.get("routes.dat");
		try (BufferedReader br = Files.newBufferedReader(path2, Charset.defaultCharset())) {
			String linha = br.readLine();
			System.out.println("Cabe�alho: " + linha);
			linha = br.readLine();
			System.out.println("Data: " + linha);
			while ((linha = br.readLine()) != null) {
				Scanner sc = new Scanner(linha).useDelimiter(";"); 
				String airline, from, to, codeshare, stops, equipment;
				Aeroporto origem, destino;
				Aeronave aeronave;
				airline = sc.next();
				CiaAerea cia = gerCias.buscarCodigo(airline);
				from = sc.next();
				origem = gerAero.buscarCodigo(from);
				to = sc.next();
				destino = gerAero.buscarCodigo(to); 
				codeshare = sc.next();
				stops = sc.next();
				equipment = sc.next();						
				aeronave = GerAer.buscarCodigo(equipment.substring(0,3));
				Rota nova = new Rota(cia, origem, destino, aeronave);
				rotas.add(nova);
				//System.out.println(airline + " - " + from + " - " + to +  " - " + codeshare +  " - " + stops + " - " + equipment);
				//System.out.println(cia + " - " + origem + " - " + destino + " - " + aeronave);
			}
		}
		System.out.println("Total rotas: " + rotas.size());
	}
	
	public void ordenarCia() {
		rotas.sort( (Rota r1, Rota r2)
				-> r1.getCia().getNome().compareTo(r2.getCia().getNome()));
	}
	
	public void ordenarOrigem() {
		rotas.sort( (Rota r1, Rota r2)
				-> r1.getOrigem().getNome().compareTo(r2.getOrigem().getNome()));
	}
	
	public void ordenarOrigemCia() {
		rotas.sort( (Rota r1, Rota r2) ->
		{
			int res = r1.getOrigem().getNome().compareTo(r2.getOrigem().getNome());
			if(res != 0)
				return res;
			// Desempata pelo nome da cia.
			return r1.getCia().getNome().compareTo(r2.getCia().getNome());
		});
		// ou:
		rotas.sort( Comparator.comparing((Rota r) -> r.getOrigem().getNome())
				.thenComparing(r -> r.getCia().getNome()));
	}
	
	public ArrayList<Rota> listarTodas() {
		return new ArrayList<Rota>(rotas);
	}
	
	public ArrayList<Rota> buscarOrigem(Aeroporto origem) {
		ArrayList<Rota> lista = new ArrayList<>();
		for(Rota r : rotas) {
			if(origem.getCodigo().equals(r.getOrigem().getCodigo()))
				lista.add(r);					
		}
		return lista;
	}
}
