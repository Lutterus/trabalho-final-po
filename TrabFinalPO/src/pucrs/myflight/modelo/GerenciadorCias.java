package pucrs.myflight.modelo;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class GerenciadorCias {
	private ArrayList<CiaAerea> empresas;

	public GerenciadorCias() {
		empresas = new ArrayList<>();
	}

	public void adicionar(CiaAerea cia) {
		empresas.add(cia);
	}

	public void carregaAirLines() throws IOException {
		Path path2 = Paths.get("airlines.dat");
		try (BufferedReader br = Files.newBufferedReader(path2, Charset.defaultCharset())) {
			String linha = br.readLine();
			System.out.println("Cabe�alho: " + linha);
			while ((linha = br.readLine()) != null) {
				Scanner sc = new Scanner(linha).useDelimiter(";"); 
																	
				String codigo, nome;
				codigo = sc.next();
				nome = sc.next();
				CiaAerea nova = new CiaAerea(codigo, nome);
				empresas.add(nova);
				//System.out.println(codigo + " - " + nome);
			}
		}
		System.out.println("Total CiaAereas: " + empresas.size());
	}
	
	public ArrayList<String> listarTodas() {
		 ArrayList<String> nova = new ArrayList<>();
		 for(CiaAerea cia: empresas)
		 nova.add(cia.getNome());
		 return nova;
	}

	public CiaAerea buscarCodigo(String codigo) {
		for (CiaAerea c : empresas) {
			if (codigo.equals(c.getCodigo()))
				return c;
		}
		return null; // não achamos!
	}

	public CiaAerea buscarNome(String nome) {
		for (CiaAerea c : empresas) {
			if (nome.equals(c.getNome()))
				return c;
		}
		return null; // não achamos!
	}
	public ObservableList getCias(){
		return FXCollections.observableList(listarTodas());
	}
}
