package pucrs.myflight.gui;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

import javax.swing.SwingUtilities;

import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.viewer.GeoPosition;

import javafx.application.Application;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import pucrs.myflight.modelo.GerenciadorAeronaves;
import pucrs.myflight.modelo.GerenciadorAeroportos;
import pucrs.myflight.modelo.GerenciadorCias;
import pucrs.myflight.modelo.GerenciadorRotas;

public class JanelaFX extends Application {

	final SwingNode mapkit = new SwingNode();

	private GerenciadorCias gerCias;
	private GerenciadorAeroportos gerAero;
	private GerenciadorRotas gerRotas;
	private GerenciadorAeronaves GerAer;
	private GerenciadorMapa gerenciador;
	private EventosMouse mouse;
	private ComboBox cb;
	private Consultas consulta;
	

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		setup();
		
		GeoPosition poa = new GeoPosition(-30.05, -51.18);
		gerenciador = new GerenciadorMapa(poa, GerenciadorMapa.FonteImagens.VirtualEarth);
		mouse = new EventosMouse();
		gerenciador.getMapKit().getMainMap().addMouseListener(mouse);
		gerenciador.getMapKit().getMainMap().addMouseMotionListener(mouse);
		createSwingContent(mapkit);
		cb = new ComboBox(gerCias.getCias());
		consulta = new Consultas(gerenciador, gerAero, gerRotas);
		BorderPane pane = new BorderPane();			
		GridPane leftPane = new GridPane();
		TextField DistanciaTextField = new TextField();
		leftPane.setAlignment(Pos.CENTER);
		leftPane.setHgap(10);
		leftPane.setVgap(10);
		leftPane.setPadding(new Insets(10,10,10,10));
		Button btnConsulta = new Button("Consulta");
		Button btnConsulta1 = new Button("Consulta 1");
		Button btnConsulta2 = new Button("Consulta 2");
		Button btnConsulta3 = new Button("Consulta 3");
		Button btnConsulta4 = new Button("Consulta 4");
		leftPane.add(btnConsulta, 0,0);
		leftPane.add(btnConsulta1, 0,0);
		leftPane.add(btnConsulta2, 0, 1);
		leftPane.add(DistanciaTextField, 0, 2);
		leftPane.add(btnConsulta3, 0, 3);
		leftPane.add(btnConsulta4, 0, 4);
		leftPane.add(cb, 0, 5);	
		
		btnConsulta1.setOnAction(e -> {
			consulta.Consulta1();
		});
		DistanciaTextField.setOnAction(e -> {
		});
		btnConsulta2.setOnAction(e -> {
			DistanciaTextField.getSelectedText();
			consulta.Consulta2(DistanciaTextField);
		});
		
		btnConsulta3.setOnAction(e -> {
			consulta.Consulta3();
		});
		btnConsulta4.setOnAction(e -> {
			consulta.Consulta4(handleCB(e));
		});
		pane.setCenter(mapkit);
		pane.setLeft(leftPane);

		Scene scene = new Scene(pane, 500, 500);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Mapas com JavaFX");
		primaryStage.show();

	}

    // Inicializando os dados aqui...
	 private void setup() {

    	gerCias = new GerenciadorCias();		
		try {
			gerCias.carregaAirLines();
		} catch (IOException e) {
			System.out.println("Imposs�vel ler airlines.dat!");
			System.out.println("Msg: "+e);
			System.exit(1);
		}
		
		GerAer = new GerenciadorAeronaves();
		try{
			GerAer.carregaAeronaves();
			
		}catch (IOException e) {
			System.out.println("Imposs�vel gravar equipment.dat");
			System.out.println("Msg: "+e);
			System.exit(1);
			
		}
		gerAero = new GerenciadorAeroportos();
		try{
			gerAero.carregaAeronaves();
		}catch (IOException e) {
			System.out.println("Imposs�vel gravar airports.dat");
			System.out.println("Msg: "+e);
			System.exit(1);
		}
		
		gerRotas = new GerenciadorRotas();
		
		try{
			gerRotas.carregaRotas(gerCias, gerAero, GerAer);			
		}catch(IOException e) {
			System.out.println("Imposs�vel gravar routes.dat");
			System.out.println("Msg: "+e);
			System.exit(1);
		}
	}
  
	private void createSwingContent(final SwingNode swingNode) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				swingNode.setContent(gerenciador.getMapKit());
			}
		});
	}

	private class EventosMouse extends MouseAdapter {
		private int lastButton = -1;

		@Override
		public void mousePressed(MouseEvent e) {
			JXMapViewer mapa = gerenciador.getMapKit().getMainMap();
			GeoPosition loc = mapa.convertPointToGeoPosition(e.getPoint());
			// System.out.println(loc.getLatitude()+", "+loc.getLongitude());
			lastButton = e.getButton();
			// Botão 3: seleciona localização
			if (lastButton == MouseEvent.BUTTON3) {
				gerenciador.setPosicao(loc);
				// gerenciador.getMapKit().setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
				gerenciador.getMapKit().repaint();
			}
		}
	}
	
	public Object handleCB(ActionEvent event){
	     Object textoDoCombo = cb.getValue();
	     return textoDoCombo;
    }
	
	public static void main(String[] args) {
		launch(args);			
	}

}
