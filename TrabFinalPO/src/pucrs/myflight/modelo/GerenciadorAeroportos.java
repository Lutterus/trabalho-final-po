package pucrs.myflight.modelo;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class GerenciadorAeroportos {

	private ArrayList<Aeroporto> aeroportos;
	
	public GerenciadorAeroportos() {
		aeroportos = new ArrayList<>();
	}
	
	public void adicionar(Aeroporto a) {
		aeroportos.add(a);	
	}
	
	public void carregaAeronaves() throws IOException{
		Path path2 = Paths.get("airports.dat");
		try (BufferedReader br = Files.newBufferedReader(path2, Charset.defaultCharset())) {
			String linha = br.readLine();
			System.out.println("Cabe�alho: " + linha);
			while ((linha = br.readLine()) != null) {
				Scanner sc = new Scanner(linha).useDelimiter(";"); 
				
				String codigo, airportName, pais;
				double latitude, longitude;
				codigo = sc.next();
				latitude = Double.parseDouble(sc.next());
				longitude = Double.parseDouble(sc.next());
				airportName = sc.next();
				Geo geo = new Geo(latitude, longitude);
				pais = sc.next();
				Aeroporto novo = new Aeroporto(codigo, airportName, geo, pais);
				aeroportos.add(novo);
				//System.out.println(codigo + " - " + airportName + " - " +  geo + " - " + pais);
			}
		}
		System.out.println("Total aeronaves: " + aeroportos.size());
	}
	
	public ArrayList<Aeroporto> listarTodos() {
		return new ArrayList<Aeroporto>(aeroportos);
	}
	
	public Aeroporto buscarCodigo(String codigo) {
		for(Aeroporto a : aeroportos) {
			if(codigo.equals(a.getCodigo()))
				return a;					
		}
		return null; // não achamos!
	}
}
