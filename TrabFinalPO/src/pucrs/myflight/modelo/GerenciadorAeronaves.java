package pucrs.myflight.modelo;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class GerenciadorAeronaves {

	private ArrayList<Aeronave> aeronaves;
	
	public GerenciadorAeronaves() {
		aeronaves = new ArrayList<>();
	}
	
	public void adicionar(Aeronave av) {
		aeronaves.add(av);	
	}
	
	public void carregaAeronaves() throws IOException{
		Path path2 = Paths.get("equipment.dat");
		try (BufferedReader br = Files.newBufferedReader(path2, Charset.defaultCharset())) {
			String linha = br.readLine();
			System.out.println("Cabe�alho: " + linha);
			while ((linha = br.readLine()) != null) {
				Scanner sc = new Scanner(linha).useDelimiter(";"); 
				
				String id, descricao;
				int capacidade;
				id = sc.next();
				descricao = sc.next();
				capacidade = sc.nextInt();
				Aeronave nova = new Aeronave(id, descricao, capacidade);
				aeronaves.add(nova);
			}
		}
		System.out.println("Total aeronaves: " + aeronaves.size());
	}
	
	public void ordenarAeronaves() {
		Collections.sort(aeronaves);
	}
	
	public void ordenarDescricao() {
//		aeronaves.sort( (Aeronave a1, Aeronave a2)
//				-> a1.getDescricao().compareTo(a2.getDescricao()) );
		//aeronaves.sort(Comparator.comparing(a -> a.getDescricao()));
		aeronaves.sort(Comparator.comparing(Aeronave::getDescricao));
	}
	
	public void ordenarCodigo() {
//		aeronaves.sort( (Aeronave a1, Aeronave a2)
//				-> a1.getCodigo().compareTo(a2.getCodigo()));
//		aeronaves.sort(Comparator.comparing(a -> a.getCodigo()));
		aeronaves.sort(Comparator.comparing(Aeronave::getCodigo));
	}
	
	public ArrayList<Aeronave> listarTodas() {
		return new ArrayList<Aeronave>(aeronaves);
	}
	
	public Aeronave buscarCodigo(String codigo) {
		for(Aeronave av : aeronaves) {
			if(codigo.equals(av.getCodigo()))
				return av;					
		}
		return null; // não achamos!
	}
}
