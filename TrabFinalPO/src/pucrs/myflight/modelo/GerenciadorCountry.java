package pucrs.myflight.modelo;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class GerenciadorCountry {
	private ArrayList<Country> pais;

	public GerenciadorCountry() {
		pais = new ArrayList<>();
	}

	public void adicionar(Country c) {
		pais.add(c);
	}

	public void carregaAirLines() throws IOException {
		Path path2 = Paths.get("airlines.dat");
		try (BufferedReader br = Files.newBufferedReader(path2, Charset.defaultCharset())) {
			String linha = br.readLine();
			System.out.println("Cabe�alho: " + linha);
			while ((linha = br.readLine()) != null) {
				Scanner sc = new Scanner(linha).useDelimiter(";"); 
																	
				String codigo, nome;
				codigo = sc.next();
				nome = sc.next();
				Country nova = new Country(codigo, nome);
				pais.add(nova);
				System.out.println(codigo + " - " + nome);
			}
		}
		System.out.println("Total paises: " + pais.size());
	}
	
	public ArrayList<Country> listarTodas() {
		// ArrayList<CiaAerea> nova = new ArrayList<>();
		// for(CiaAerea cia: empresas)
		// nova.add(cia);
		// return nova;
		return new ArrayList<Country>(pais);
	}

	public Country buscarCodigo(String codigo) {
		for (Country c : pais) {
			if (codigo.equals(c.getCodigo()))
				return c;
		}
		return null; // não achamos!
	}

	public Country buscarNome(String nome) {
		for (Country c : pais) {
			if (nome.equals(c.getNome()))
				return c;
		}
		return null; // não achamos!
	}
	
}